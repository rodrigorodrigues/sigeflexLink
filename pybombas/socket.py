import time

__author__ = 'Rodrigo'

import socket  # for sockets


class Socket():
    __connection = None
    tryConnect = 0

    def __init__(self):
        self.waiting = False

    def connect(self, host, port):
        self.host = host
        self.port = port
        # create an INET, STREAMing socket
        try:
            self.__connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__connection.connect((host, port))
            retorno = self.__connection.recv(4096)
            self.tryConnect = 0
            return "Connected at " + host + ":" + str(port) + ' Retorno : ' + str(retorno)
        except Exception as e:
            raise Exception(e)

    def send(self, message, checkResponseStr=None):
        try:
            self.waiting = True
            message = str.encode(message)
            self.__connection.send(message)
            time.sleep(0.1)
            data = ""
            reply = ""
            while reply.strip() == "":
                reply = self.__connection.recv(4096)
                reply = reply.decode('utf-8')

                replyNew = ''
                for s in reply:
                    if ord(s) > 7:
                        replyNew += s
                reply = replyNew

                if checkResponseStr:
                    if not checkResponseStr in reply:
                        reply = None

            self.waiting = False
            return str(reply)
        except socket.timeout:
            self.waiting = False
            if self.host and self.port and self.tryConnect < 4:
                self.connect(self.host, self.port)

        except Exception as e:
            self.waiting = False
            raise Exception(e)
