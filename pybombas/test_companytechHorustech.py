from pybombas.concentrador import Concentrador

__author__ = 'rodrigo'
import json

from .companytechHorustech import CompanytechHorustech

horustech = CompanytechHorustech()


def test_checksum():
    tests = [["!000A01AALB P A", "34"],
             ["?000808000089", "a0"]]
    for test in tests:
        assert (horustech.calcChecksum(test[0]) == test[1])


def test_calcbytes():
    tests = [["01AALB P A", "000A"],
             ["08000089", "0008"]]

    for test in tests:
        assert (horustech.calcBytes(test[0]) == test[1])


def test_gera_comando():
    tests = [["0608", ">?00040608D1"],
             ["0208856", ">?0007020885673"]]

    for test in tests:
        assert (horustech.geraComando(test[0]) == test[1])


def test_le_resposta_abastecimento():
    tests = [[
        ">!006B0800008907000000100100299133502330033060715130800525911420052591441000000000000000000000000000000009D153496ED",
        '{"codigo":"060715130889","numeroAbastecimento":89,"numeroBico":"07","codigoCombustivel":"00","combustivel":"","numeroTanque":"00","totalPagar":10.01,"volumeAbastecido":2.991,"precoUnitario":3.35,"tempoAbastecimento":"0033","dataHora":"06/07/2015 13:08","encerranteInicial":525911.42,"encerranteFinal":525914.41,"codigoFrentista":"0000000000000000","codigoCliente":"0000000000000000","volumeTanque":"9D153496"}'],
        [
            ">!006B080000870500000020020059793350233007506071513060039800337003980093500000000000000000000000000000000D9A8B9C633",
            '{"codigo":"060715130687","numeroAbastecimento":87,"numeroBico":"05","codigoCombustivel":"00","combustivel":"","numeroTanque":"00","totalPagar":20.02,"volumeAbastecido":5.979,"precoUnitario":3.35,"tempoAbastecimento":"0075","dataHora":"06/07/2015 13:06","encerranteInicial":398003.37,"encerranteFinal":398009.35,"codigoFrentista":"0000000000000000","codigoCliente":"0000000000000000","volumeTanque":"D9A8B9C6"}'],
        [
            ">!006B08000088030000003600010746335023300780607151306001748961300174906870000000000000000000000000000000065F2A74E23",
            '{"codigo":"060715130688","numeroAbastecimento":88,"numeroBico":"03","codigoCombustivel":"00","combustivel":"","numeroTanque":"00","totalPagar":36,"volumeAbastecido":10.746,"precoUnitario":3.35,"tempoAbastecimento":"0078","dataHora":"06/07/2015 13:06","encerranteInicial":174896.13,"encerranteFinal":174906.87,"codigoFrentista":"0000000000000000","codigoCliente":"0000000000000000","volumeTanque":"65F2A74E"}']]

    for test in tests:
        esperado = json.loads(test[1])

        retorno = horustech.leAbastecimento(test[0])

        assert retorno['codigo'] == esperado['codigo']
        assert retorno['numeroAbastecimento'] == esperado['numeroAbastecimento']
        assert retorno['numeroBico'] == esperado['numeroBico']
        assert retorno['codigoCombustivel'] == esperado['codigoCombustivel']
        assert retorno['combustivel'] == esperado['combustivel']
        assert retorno['numeroTanque'] == esperado['numeroTanque']
        assert retorno['totalPagar'] == esperado['totalPagar']
        assert retorno['volumeAbastecido'] == esperado['volumeAbastecido']
        assert retorno['precoUnitario'] == esperado['precoUnitario']
        assert retorno['tempoAbastecimento'] == esperado['tempoAbastecimento']
        assert retorno['dataHora'].strftime('%d/%m/%Y %H:%M') == esperado['dataHora']
        assert retorno['encerranteInicial'] == esperado['encerranteInicial']
        assert retorno['encerranteFinal'] == esperado['encerranteFinal']
        assert retorno['codigoFrentista'] == esperado['codigoFrentista']
        assert retorno['codigoCliente'] == esperado['codigoCliente']
        assert retorno['volumeTanque'] == esperado['volumeTanque']


def test_le_tatus_bicos():
    tests = [[">!000A0306003922EB", [{"numeroBico": "06", "totalPagar": 39.22}]],
             [">!00020346", []],
             [">!000A0309002942EF", [{"numeroBico": "09", "totalPagar": 29.42}]],
             [">!000A0305000379ED", [{"numeroBico": "05", "totalPagar": 3.79}]],
             [">!000A0305000414E3", [{"numeroBico": "05", "totalPagar": 4.14}]],
             [">!000A0305000445E7", [{"numeroBico": "05", "totalPagar": 4.45}]],
             [">!000A0303002938EE", [{"numeroBico": "03", "totalPagar": 29.38}]],
             [">!00120305001270010050005C",
              [{"numeroBico": "05", "totalPagar": 12.70}, {"numeroBico": "01", "totalPagar": 50}]],
             [">!000A0305002323E4", [{"numeroBico": "05", "totalPagar": 23.23}]],
             [">!000A0307000000DC", [{"numeroBico": "07", "totalPagar": 0}]]]

    for test in tests:
        assert horustech.leAbastecimentoEmAndamento(test[0]) == test[1]


def test_ler_abastecimento_concentrador():
    concentrador = Concentrador()
    abastecimento = concentrador.leAbastecimento()
    assert abastecimento
