import json

import time

from pybombas import CompanytechHorustech
from pybombas.socket import Socket

TIPO_CONEXAO = (
    ('socket', 'Socket'),
    ('serial', 'Serial')
)

MODELO_CONCENTRADOR = (
    ('COMPANYTECH_HORUSTECH', 'Companytech Horustech'),
)


class Concentrador():
    AUTOMACAO_STARTADA = False
    conexao = None
    configuracao = None
    concentrador = None
    Timer = None
    modelo = MODELO_CONCENTRADOR[0][0]
    tipo_conexao = 'socket'
    host = 'localhost'
    porta = ''

    def configConcentrador(self, configuracao):

        with open('config_concentrador', 'w') as f:
            f.write(configuracao)

        Concentrador.configuracao = configuracao

        # # Create ou update a configuacao
        # bombas, created = Concentrador.objects.update_or_create(
        #     id=1,
        #     defaults={'concentrador_host': configuracao['host'],
        #               'concentrador_porta': configuracao['port'],
        #               'concentrador_modelo': configuracao['concentrador'],
        #               'concentrador_tipoConexao': configuracao['tipoConexao']},
        # )
        #
        # if bombas.concentrador_modelo not in Concentrador.MODELO_CONCENTRADOR:
        #     return ''' Os concentradores compatíveies são
        #               %s+.
        #               No entanto foi enviado o seguinte modelo para integracao : %s
        #           ''' % (str(Concentrador.MODELO_CONCENTRADOR), bombas.concentrador_modelo)
        #
        # # Grava os dados dos bicos
        # bicos = None
        # try:
        #     bicos = configuracao['bicos']
        # except:
        #     pass
        #
        # if bicos:
        #     for bico in bicos:
        #         Bico.objects.update_or_create(
        #             numero=bico['numero'],
        #             defaults={'combustivel': bico['combustivel'],
        #                       'codigoCombustivel': bico['combustivel'],
        #                       'numero': bico['numero'],
        #                       'numeroTanque': bico['numeroTanque']},
        #         )
        #
        #     # Traga a confiuracao de bicos do banco de dados
        #     bicos = Bico.objects.all()
        #     for bico in bicos:
        #         # Atualiza os abastecimentos no campo combustivel e tanque de acordo com as configuracoes do bico passada
        #         Abastecimento.objects.filter((~Q(combustivel=bico.get_combustivel_display()) |
        #                                       ~Q(numeroTanque=bico.numeroTanque)),
        #                                      numeroBico=bico.numero
        #                                      ).update(combustivel=bico.get_combustivel_display(),
        #                                               numeroTanque=bico.numeroTanque)
        #
        # # Grava os dados dos frentistas
        # try:
        #     frentistas = configuracao['frentistas']
        #     for frentista in frentistas:
        #         Frentista.objects.update_or_create(
        #             codigo=frentista['codigo'],
        #             defaults={'nome': frentista['nome'],
        #                       'codigo': frentista['codigo']},
        #         )
        # except:
        #     pass
        #
        # # Salva a configuracao no banco de dados, seja insert ou update
        # self.start()
        # return "Concentrador configurado"
        pass

    # Realiza a conexao com o concentrador
    @classmethod
    def configConexao(cls):

        config = Concentrador.configuracao
        try:

            # Instancie a classe do concentrador
            if not config:
                with open('config_concentrador.json') as f:
                    config = json.loads(f.read())

            if config.get('modelo') == 'COMPANYTECH_HORUSTECH':
                Concentrador.concentrador = CompanytechHorustech()
            else:
                raise Exception(
                    "O concentrador configurado : %s não tem suporte " % (
                        Concentrador.configuracao.concentrador_modelo))

            if not Concentrador.conexao:
                Concentrador.conexao = Socket()
                Concentrador.conexao.connect(config.get('host'),
                                             config.get('porta'))
                Concentrador.conexao.connected = 1

        except Exception as e:

            if '10060' in str(e):
                raise Exception("Nao foi possivel conectar ao concentrador")
            else:
                raise Exception(e)

    @classmethod
    def leTotais(cls, numeroBico):
        try:
            cls.configConexao()
            comando = Concentrador.concentrador.getComandoTotais(numeroBico)
            resposta = []
            if (cls.conexao.connected):
                resposta = cls.enviaComando(comando)
                resposta = Concentrador.concentrador.leTotais(resposta)
        except Exception as e:
            raise Exception(''' Erro ao processar comando!
                                comando enviado : %s
                                resposta : %s''' % (comando, resposta))

        return resposta

    def leAbastecimentoIntervalo(self, inicial, final):
        lidos = []
        try:
            while inicial <= final:
                retorno = self.leAbastecimento(inicial)
                if retorno != None:
                    if not isinstance(retorno, str):
                        lidos.append("NumAbastecimento :" + str(retorno.numeroAbastecimento) + " DataHora : " + str(
                            retorno.dataHora))
                inicial += 1
        except Exception as e:
            raise Exception(e)
        return lidos

    @classmethod
    def leAbastecimentoEmAndamento(cls):

        cls.configConexao()

        comando = Concentrador.concentrador.getComandoAbastecimentoEmAndamento()
        resposta = cls.enviaComando(comando)
        resposta = Concentrador.concentrador.leAbastecimentoEmAndamento(resposta)

        return resposta

    @classmethod
    def enviaComando(cls, comando):

        while cls.conexao.waiting:
            time.sleep(1)

        return cls.conexao.send(comando)

    def leAbastecimento(cls, numero=None, incrementa=None):

        cls.configConexao()

        comandoLerAbastecimento = Concentrador.concentrador.getComandoAbastecimento(numero)
        comandoIncremento = Concentrador.concentrador.getComandoIncremento()

        resposta = cls.enviaComando(comandoLerAbastecimento)
        resposta = Concentrador.concentrador.leAbastecimento(resposta)

        if (resposta == None):
            return "Sem abastecimento"

        if incrementa:
            cls.enviaComando(comandoIncremento)

        return resposta

