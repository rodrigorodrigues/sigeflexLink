#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodrigo Rodrigues'
import datetime


class CompanytechHorustech():
    def calcChecksum(self, codigo):
        somatorio = 0

        for letra in codigo:
            somatorio += ord(letra)

        hexValue = hex(somatorio)
        if len(hexValue) > 2:
            hexValue = hexValue[-2:]

        return hexValue

    def calcBytes(self, str):
        length = len(str)
        bytes = hex(length)
        bytes = bytes[2:]
        return bytes.zfill(4).upper()

    def geraComando(self, comando):
        comando = '?' + self.calcBytes(comando) + comando
        return ('>' + comando + self.calcChecksum(comando).upper())

    def formataResposta(self, resposta):
        return resposta[8:]

    def getComandoIncremento(self):
         return '>?00020667'

    def getComandoAbastecimento(self, numero):
        if (numero == None):
            return self.geraComando('02')
        else:
            return self.geraComando('08' + str(numero).zfill(6))

    def getComandoAbastecimentoEmAndamento(self):
        return self.geraComando('03')

    def leAbastecimento(self, respostaStr):

        try:
            abastecimento = {}
            if (respostaStr == ">!00020245\r"):
                return None

            resposta = self.formataResposta(respostaStr)
            abastecimento['numeroAbastecimento'] = int(resposta[:6])
            abastecimento['numeroBico'] = resposta[6:8]
            abastecimento['codigoCombustivel'] = resposta[8:10]
            abastecimento['combustivel'] = ''
            abastecimento['numeroTanque'] = resposta[10:12]
            abastecimento['totalPagar'] = float(resposta[12:18]) / 100
            abastecimento['volumeAbastecido'] = float(resposta[18:24]) / 1000
            abastecimento['precoUnitario'] = float(resposta[24:28]) / 1000
            abastecimento['tempoAbastecimento'] = resposta[31:35]

            dataHora = resposta[35:45]
            dataHora = "dd/mm/20yy hh:MM".replace("dd", dataHora[0:2]) \
                .replace("mm", dataHora[2:4]) \
                .replace("yy", dataHora[4:6]) \
                .replace("hh", dataHora[6:8]) \
                .replace("MM", dataHora[8:10])
            abastecimento['dataHora'] = datetime.datetime.strptime(dataHora, "%d/%m/%Y %H:%M")

            abastecimento['codigo'] = resposta[35:45] + str(abastecimento['numeroAbastecimento'])
            abastecimento['encerranteInicial'] = float(resposta[45:55]) / 100
            abastecimento['encerranteFinal'] = float(resposta[55:65]) / 100
            abastecimento['codigoFrentista'] = resposta[65:81]
            abastecimento['codigoCliente'] = resposta[81:97]
            abastecimento['volumeTanque'] = resposta[97:105]
            return abastecimento
        except Exception as e:
               return None
               #raise Exception('Resposta str : '+respostaStr+'  Resposta processada : '+resposta)

    def getComandoTotais(self,numeroBico):
        return self.geraComando('05%s%s'%(str(numeroBico).zfill(2),'04'))

    def leTotais(self, resposta):
        resposta = resposta[8:-2]
        totais = {}
        totais['numeroBico'] = resposta[:2]
        totais['encerranteLitros'] = float(resposta[4:14]) / 100
        totais['encerranteValor'] = float(resposta[14:24]) / 100
        totais['precoUnitario'] = float(resposta[24:28]) / 1000
        return totais


    def leAbastecimentoEmAndamento(self, resposta):

        # retira os 8 primeiros caracteres e os dois últimos(checksum) caracretes Ex : >!000A0306003922EB, somente a parte 06003922 é o que importa
        resposta = resposta[8:-2]
        quantidadeBicosAbastecendo = int(len(resposta) / 8)

        bicos = []
        for i in range(quantidadeBicosAbastecendo):
            bico = {'numeroBico': resposta[0:2], 'totalPagar': resposta[2:8]}
            bico['totalPagar'] = float(bico['totalPagar']) / 100
            bicos.append(bico)
            resposta = resposta[8:]

        return bicos
