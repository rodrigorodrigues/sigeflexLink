import socket
from time import sleep

__author__ = 'Rodrigo'


def formataValor(valor):
    subtotal = valor.replace(',', '.').strip()
    subtotal = float(subtotal)
    return subtotal


class Socket:
    def __init__(self):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host, port):
        self.mySocket.connect((host, port))

        # self.mySocket.listen(1)

    def send(self, command):
        return self.mySocket.send(command.encode())

    def close(self):
        self.mySocket.close()


class Impressora:

    def enviaComando_acbr(self, comando, conexao):
        retorno = conexao.send(str(comando + '\r\n.\r\n'))
        sleep(0.3)

        if isinstance(retorno, list):
            for s in retorno:
                retornoNovo = ''
                if ord(s) >= 32:
                    retornoNovo += s

                retorno = retornoNovo

        return retorno

    def abrir_gaveta_acbr(self):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)
        try:
            self.enviaComando_acbr('GAV.AbreGaveta', conexao)
        finally:
            conexao.close()

    def imprime_acbr(self, texto):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)



        def imprime_texto(orientacao='normal', numero_fonte=3, multiplicador_horizontal=1,
                          multiplicador_vertical=1,
                          posicao_vertical=160, posicao_horizontal=720, texto='', copias=1, avanco=0):

            if orientacao == 'normal':
                orientacao = 0

            comando = 'ETQ.ImprimirTexto( {orientacao}, {numero_fonte}, {multiplicador_horizontal}, {multiplicador_vertical}, {posicao_vertical}, {posicao_horizontal}, "{texto}")'.format(
                orientacao=orientacao,
                numero_fonte=numero_fonte,
                multiplicador_horizontal=multiplicador_horizontal,
                multiplicador_vertical=multiplicador_vertical,
                posicao_vertical=posicao_vertical,
                posicao_horizontal=posicao_horizontal,
                texto=texto)

            enviaComando(comando, conexao)

        enviaComando('ESCPOS.Ativar', conexao)
        try:
            # for etiqueta in etiquetas:
                # for campo in etiqueta.get('campos'):
                #     imprime_texto(orientacao=campo.get('orientacao'),
                #                        posicao_vertical=campo.get('posicao_vertical'),
                #                        numero_fonte=campo.get('tamanho_fonte'),
                #                        posicao_horizontal=campo.get('posicao_horizontal'),
                #                        multiplicador_vertical=campo.get('altura_fonte'),
                #                        multiplicador_horizontal=campo.get('largura_fonte'),
                #                        texto=campo.get('texto'))
                #
                # enviaComando('ETQ.Imprimir( {0}, {1})'.format(1, 600))
            filename = 'C:\\Users\\Rodrigo\\projetos\\pyutil\impressora\\24170919843657000149650010000038311000038310-nfce.xml'
          #   enviaComando(
          #           'ESCPOS.imprimir("</zera></linha_dupla>FONTE NORMAL: 48 Colunas</lf>....+....1....+....2....+....3....+....4....+...</lf><e>EXPANDIDO: 24 Colunas</lf>....+....1....+....2....</lf></e><c>CONDENSADO: 64 Colunas</lf>....+....1....+....2....+....3....+....4....+....5....+....6....</lf></pular_linhas></corte_total>')


            enviaComando('MDFE.IMPRIMIRDAMDFE({0})'.format(filename), conexao)


        finally:
            pass
            conexao.close()
