import socket
from time import sleep

__author__ = 'Rodrigo'


def formataValor(valor):
    subtotal = valor.replace(',', '.').strip()
    subtotal = float(subtotal)
    return subtotal


class Socket:
    def __init__(self):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host, port):
        self.mySocket.connect((host, port))

        # self.mySocket.listen(1)

    def send(self, command):
        return self.mySocket.send(command.encode())

    def close(self):
        self.mySocket.close()


class Etiqueta:



    def imprime_etiquetas_acbr(self, etiquetas):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)

        def enviaComando(comando):
            retorno = conexao.send(str(comando + '\r\n.\r\n'))
            sleep(0.3)

            if isinstance(retorno, list):
                for s in retorno:
                    retornoNovo = ''
                    if ord(s) >= 32:
                        retornoNovo += s

                    retorno = retornoNovo

            return retorno

        def imprime_texto(orientacao='normal', numero_fonte=3, multiplicador_horizontal=1,
                          multiplicador_vertical=1,
                          posicao_vertical=160, posicao_horizontal=720, texto='', copias=1, avanco=0):

            if orientacao == 'normal':
                orientacao = 0

            comando = 'ETQ.ImprimirTexto( {orientacao}, {numero_fonte}, {multiplicador_horizontal}, {multiplicador_vertical}, {posicao_vertical}, {posicao_horizontal}, "{texto}")'.format(
                orientacao=orientacao,
                numero_fonte=numero_fonte,
                multiplicador_horizontal=multiplicador_horizontal,
                multiplicador_vertical=multiplicador_vertical,
                posicao_vertical=posicao_vertical,
                posicao_horizontal=posicao_horizontal,
                texto=texto)

            enviaComando(comando)

        enviaComando('ETQ.Ativar')
        try:
            for etiqueta in etiquetas:
                for campo in etiqueta.get('campos'):
                    imprime_texto(orientacao=campo.get('orientacao'),
                                       posicao_vertical=campo.get('posicao_vertical'),
                                       numero_fonte=campo.get('tamanho_fonte'),
                                       posicao_horizontal=campo.get('posicao_horizontal'),
                                       multiplicador_vertical=campo.get('altura_fonte'),
                                       multiplicador_horizontal=campo.get('largura_fonte'),
                                       texto=campo.get('texto'))

                enviaComando('ETQ.Imprimir( {0}, {1})'.format(1, 600))
        finally:
            pass
            conexao.close()


    def peso(self):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)

        def enviaComando(comando):
            retorno = conexao.send(str(comando + '\r\n.\r\n'))
            sleep(0.3)

            if isinstance(retorno, list):
                for s in retorno:
                    retornoNovo = ''
                    if ord(s) >= 32:
                        retornoNovo += s

                    retorno = retornoNovo

            return retorno
        try:
            enviaComando('BAL.Ativar')
            return enviaComando('BAL.LePeso')
        finally:
            pass
            conexao.close()





class ECFAcbrMonitor():
    conexao = Socket()

    def configura(self):
        self.conexao.connect('localhost', 3434)

    def abreCupom(self, dados):
        return self.enviaComando('ECF.AbreCupom').strip() == ''

    def vendeItem(self, dados):
        comando = '"codigo", "descricao", aliquota, quantidade, valorUnitario, valorDescAcrescimo, "unidade", "tipoDescontoAcrescimo", descontoOuAcrescimo'
        comando = comando.replace('codigo', dados['codigo'])
        comando = comando.replace('descricao', dados['descricao'])
        comando = comando.replace('aliquota', dados['aliquota'])
        comando = comando.replace('valorUnitario', str(dados['valor_unitario']))
        comando = comando.replace('quantidade', str(dados['quantidade']))
        comando = comando.replace('valorDescAcrescimo', str(dados['valor_desconto_acrescimo']))
        comando = comando.replace('unidade', dados['un'])
        comando = comando.replace('tipoDescontoAcrescimo', '$')
        if dados['valor_desconto_acrescimo'] > 0:
            comando = comando.replace('descontoOuAcrescimo', 'A')
        else:
            comando = comando.replace('descontoOuAcrescimo', 'D')

        return self.enviaComando('ECF.VendeItem(%s)' % (comando)).strip() == ''

    def fechaCupom(self, dados):
        # Subtotaliza cupom
        self.enviaComando('ECF.SubtotalizaCupom(%s)' % (dados['valorDescAcres']))

        # Efetua pagamento
        for p in dados['pagamentos']:
            self.enviaComando('ECF.EfetuaPagamento("%s", %s)' % (p['codigo'], p['valor']))

        # Fecha cupom fiscal
        return self.enviaComando('ECF.FechaCupom (%s)' % (dados['mensagem'])).strip() == ''

    def enviaComando(self, comando):
        retorno = self.conexao.send(comando + '\r\n.\r\n')
        retornoNovo = ''
        for s in retorno:
            if ord(s) >= 32:
                retornoNovo += s

        retorno = retornoNovo

        return retorno[3:]

    def status(self):
        retorno = self.enviaComando('ECF.Estado')
        retorno = retorno.strip()

        if retorno == 'estNaoInicializada':
            return 'NAOINICIALIZADA'
        elif retorno == 'estDesconhecido':
            return 'DESCONHECIDO'
        elif retorno == 'estLivre':
            return 'LIVRE'
        elif retorno == 'estVenda':
            return 'VENDA'
        elif retorno == 'estPagamento':
            return 'PAGAMENTO'
        elif retorno == 'estRelatorio':
            return 'RELATORIO'
        elif retorno == 'estBloqueada':
            return 'BLOQUEADA'
        elif retorno == 'estRequerZ':
            return 'REQUERZ'
        elif retorno == 'estRequerX':
            return 'REQUERX'

        return 'DESCONHECIDO'

    def subtotal(self):
        subtotal = self.enviaComando('ECF.SubTotal')
        subtotal = formataValor(subtotal)
        return subtotal

    def cancelaCupom(self):
        cancela = self.enviaComando('ECF.CancelaCupom').strip()
        if 'Erro' in cancela:
            return cancela

        return True

    def cancelaItem(self, numItem):
        return self.enviaComando('ECF.CancelaItemVendido(%s)' % (numItem)).strip() == ''

    def leituraX(self):
        return self.enviaComando('ECF.LeituraX')

    def reducaoZ(self):
        return self.enviaComando('ECF.ReducaoZ')

    def dataHora(self):
        return self.enviaComando('ECF.DataHora')

    def marca(self):
        return self.enviaComando('ECF.ModeloStr')

    def modelo(self):
        return self.enviaComando('ECF.SubModeloECF')

    def numEcf(self):
        return self.enviaComando('ECF.NumECF')

    def numLoja(self):
        return self.enviaComando('ECF.NumLoja')

    def numSerie(self):
        return self.enviaComando('ECF.NumSerie')

    def numSerieMfd(self):
        return self.enviaComando('ECF.NumSerieMFD')

    def numVersao(self):
        return self.enviaComando('ECF.NumVersao')

    def cnpj(self):
        return self.enviaComando('ECF.CNPJ')

    def ie(self):
        return self.enviaComando('ECF.IE')

    def im(self):
        return self.enviaComando('ECF.IM')

    def paf(self):
        return self.enviaComando('ECF.PAF')

    def usuarioAtual(self):
        return self.enviaComando('ECF.UsuarioAtual')

    def cliche(self):
        return self.enviaComando('ECF.Cliche')

    def dataHoraSB(self):
        return self.enviaComando('ECF.DataHoraSB')

    def decimaisQuantidade(self):
        return self.enviaComando('ECF.DecimaisQtd')

    def decimaisPrecoUnitario(self):
        return self.enviaComando('ECF.DecimaisPreco')

    def mfAdicional(self):
        return self.enviaComando('ECF.MFAdicional')

    def dadosReducaoZ(self):
        return self.enviaComando('ECF.DadosReducaoZ')

    def dadosUltimaReducaoZ(self):
        return self.enviaComando('ECF.DadosUltimaReducaoZ')

    def numReducoesZRestantes(self):
        return self.enviaComando('ECF.NumReducoesZRestantes')

    def numCOO(self):
        return self.enviaComando('ECF.NumCOO')

    def numCRZ(self):
        return self.enviaComando('ECF.NumCRZ')

    def numCRO(self):
        return self.enviaComando('ECF.NumCRO')

    def numCCF(self):
        return self.enviaComando('ECF.NumCCF')

    def numCOOInicial(self):
        return self.enviaComando('ECF.NumCOOInicial')

    def numGNF(self):
        return self.enviaComando('ECF.NumGNF')

    def numGNFC(self):
        return self.enviaComando('ECF.numGNFC')

    def numGRG(self):
        return self.enviaComando('ECF.numGRG')

    def numCDC(self):
        return self.enviaComando('ECF.numCDC')

    def numCFC(self):
        return self.enviaComando('ECF.numCFC')

    def numCCDC(self):
        return self.enviaComando('ECF.numCCDC')

    def numCFD(self):
        return self.enviaComando('ECF.numCFD')

    def numNCN(self):
        return self.enviaComando('ECF.numNCN')

    def vendaBruta(self):
        return self.enviaComando('ECF.VendaBruta')

    def grandeTotal(self):
        return self.enviaComando('ECF.GrandeTotal')

    def totalTroco(self):
        troco = self.enviaComando('ECF.TotalTroco')
        troco = formataValor(troco)
        return troco

    def poucoPapel(self):
        if self.enviaComando('ECF.PoucoPapel') == 'True':
            return True
        return False

    def abreRelatorioGerencial(self):
        return self.enviaComando('ECF.AbreRelatorioGerencial').strip() == ''

    def imprimeLinha(self, linha):
        comando = 'ECF.RelatorioGerencial("%s")' % (linha)
        return self.enviaComando(comando).strip() == ''

    def imprimeLinhaRelatorioGerencial(self, linha):
        comando = 'ECF.LinhaRelatorioGerencial("%s")' % (linha)
        return self.enviaComando(comando).strip() == ''

    def fechaRelatorio(self):
        return self.enviaComando('ECF.FechaRelatorio').strip() == ''

    def identifica_paf(self, nome_paf, md5):
        comando = 'ECF.IdentificaPAF("{0},{1}")'.format(nome_paf, md5)
        return self.enviaComando(comando).strip() == ''
