import json

import tornado.ioloop
import tornado.web
from tornado_cors import CorsMixin

from geral.sigelink import Sigelink

#gerar instalador: pynsist (arquivo).nsi
'''
Run pynsist installer.cfg to generate your installer. If pynsist isn’t found, 
you can use python -m nsist installer.cfg instead.
'''

class GeralController(CorsMixin, tornado.web.RequestHandler):
    CORS_ORIGIN = '*'
    CORS_HEADERS = 'Content-Type'
    CORS_METHODS = 'POST'

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Content-Type', 'application/json')
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def post(self, *args, **kwargs):
        try:
            sigelink = Sigelink()
            if hasattr(self.request, 'body') and self.request.body:
                data = json.loads(self.request.body.decode())
            else:
                data = {}
            retorno = {}

            if '/tef/crt' in self.request.path:
                retorno = sigelink.tef_crt(data)
            elif '/tef/adm' in self.request.path:
                retorno = sigelink.tef_adm(data)
            elif '/tef/ncn' in self.request.path:
                retorno = sigelink.tef_ncn(data)
            elif '/tef/cnf' in self.request.path:
                retorno = sigelink.tef_cnf(data)
            elif 'etiquetas/imprime' in self.request.path:
                retorno = sigelink.etiquetas_imprimir(data)
            elif '/peso' in self.request.path:
                retorno = sigelink.balanca_peso()
            elif '/nfe/envia' in self.request.path:
                retorno = sigelink.nfe_envia(data.get('xml',''))
            elif '/nfe/imprime' in self.request.path:
                retorno = sigelink.nfe_imprime_danfe(data)
            elif '/gaveta/abrir' in self.request.path:
                retorno = sigelink.gaveta_abrir()
            else:
                retorno = "<html><body>SIGELINK rodando...</body></html>"
            retorno = json.dumps(retorno)
            self.write(retorno)
        except Exception as e:
            self.set_status(422)
            retorno = json.dumps(['erro',str(e)])
            self.write(retorno)


    def get(self, *args, **kwargs):
        html = "SIGELINK rodando..."
        self.write(html)


def make_app():
    return tornado.web.Application([
        (r"/", GeralController),
        (r"/tef/(.*)", GeralController),
        (r"/gaveta/(.*)", GeralController),
        (r"/nfe/(.*)", GeralController),
        (r"/peso", GeralController),
        (r"/etiquetas/(.*)", GeralController),
    ])

def start():
    app = make_app()
    app.listen(5000)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    print('Sigeflex Link iniciado')
    start()

