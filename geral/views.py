import json

# Create your views here.
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from geral.sigelink import Sigelink


@csrf_exempt
def geral(request):
    sigelink = Sigelink()
    if request.method == 'POST':
        if hasattr(request, 'data'):
            data = json.loads(request.data.decode())
        else:
            data = {}
        retorno = {}
        if '/tef/crt' in request.path:
            retorno = sigelink.tef_crt(data)
        elif '/tef/adm' in request.path:
            retorno = sigelink.tef_crt(data)
        elif '/tef/ncn' in request.path:
            retorno = sigelink.tef_crt(data)
        elif '/tef/cnf' in request.path:
            retorno = sigelink.tef_crt(data)
        elif 'etiquetas/imprime' in request.path:
            retorno = sigelink.etiquetas_imprimir(request.data)
        elif '/peso' in request.path:
            retorno = sigelink.balanca_peso()
        elif '/nfe/assina' in request.path:
            retorno = sigelink.nfe_assina_xml(request.data)
        elif '/nfe/imprime' in request.path:
            retorno = sigelink.nfe_imprime_danfe(request.data)
        retorno = json.dumps(retorno)
        return JsonResponse(retorno, safe=False)
    else:
        html = "<html><body>SIGELINK rodando...</body></html>"
        return HttpResponse(html)
