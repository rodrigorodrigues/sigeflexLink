import os
import socket
from datetime import datetime
from time import sleep

from pytef.tef import Tef, TransacaoVenda


class Socket:
    def __init__(self):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connected = False

    def connect(self, host, port):
        try:
            self.mySocket.connect((host, port))
            self.connected = True
        except Exception as e:
            if isinstance(e, ConnectionRefusedError):
                raise Exception('Sem comunicação com o programa Monitor...')

        # self.mySocket.listen(1)

    def send(self, command):
        self.mySocket.sendall(command.encode())
        self.mySocket.settimeout(10)
        data = ''
        while True:
            try:
                # this is the problem here
                data += self.mySocket.recv(1024).decode()
                if 'OK:' in data:
                    return data

            except Exception as e:
                return str(e)

    def close(self):
        self.mySocket.close()


class Sigelink:

    def __init__(self):
        self.conexao = Socket()

    def enviaComando_acbr(self, comando):
        if not self.conexao.connected:
            self.conectaAcbrMonitor()

        retorno = self.conexao.send(str(comando + '\r\n.\r\n'))
        return retorno

    def conectaAcbrMonitor(self):
        self.conexao.connect('127.0.0.1', 3434)

    def etiquetas_imprimir(self, etiquetas):
        def enviaComando(comando):
            retorno = self.conexao.send(str(comando + '\r\n.\r\n'))
            sleep(0.3)

            if isinstance(retorno, list):
                for s in retorno:
                    retornoNovo = ''
                    if ord(s) >= 32:
                        retornoNovo += s

                    retorno = retornoNovo

            return retorno

        def imprime_texto(orientacao='normal', numero_fonte=3, multiplicador_horizontal=1,
                          multiplicador_vertical=1,
                          posicao_vertical=160, posicao_horizontal=720, texto='', copias=1, avanco=0):

            if orientacao == 'normal':
                orientacao = 0

            comando = 'ETQ.ImprimirTexto( {orientacao}, {numero_fonte}, {multiplicador_horizontal}, {multiplicador_vertical}, {posicao_vertical}, {posicao_horizontal}, "{texto}")'.format(
                orientacao=orientacao,
                numero_fonte=numero_fonte,
                multiplicador_horizontal=multiplicador_horizontal,
                multiplicador_vertical=multiplicador_vertical,
                posicao_vertical=posicao_vertical,
                posicao_horizontal=posicao_horizontal,
                texto=texto)

            enviaComando(comando)

        enviaComando('ETQ.Ativar')
        try:
            for etiqueta in etiquetas:
                for campo in etiqueta.get('campos'):
                    imprime_texto(orientacao=campo.get('orientacao'),
                                  posicao_vertical=campo.get('posicao_vertical'),
                                  numero_fonte=campo.get('tamanho_fonte'),
                                  posicao_horizontal=campo.get('posicao_horizontal'),
                                  multiplicador_vertical=campo.get('altura_fonte'),
                                  multiplicador_horizontal=campo.get('largura_fonte'),
                                  texto=campo.get('texto'))

                enviaComando('ETQ.Imprimir( {0}, {1})'.format(1, 600))
        finally:
            pass
            self.conexao.close()

    def balanca_peso(self):
        try:
            retorno = self.enviaComando_acbr('BAL.Ativar')
            retorno = self.enviaComando_acbr('BAL.LePeso')
            try:
                retorno = retorno[retorno.rindex(' ') + 1:].replace(',', '.')
            except:
                return '0.00'

            return retorno
        finally:
            pass
            self.conexao.close()

    def gaveta_abrir(self):
        try:
            retorno = self.enviaComando_acbr('GAV.Ativo')
            return self.enviaComando_acbr('GAV.AbreGaveta')
        finally:
            pass
            self.conexao.close()

    def nfe_assina_xml(self, arquivoxml):
        try:

            arquivo_xml_local = os.getcwd() + '\\assina-nfe.xml'
            # arquivo_xml_local = '''C:\Users\Rodrigo\projetos\pyutil\earquivo_xml.xml'''

            with open(arquivo_xml_local, 'w') as f:
                f.write(arquivoxml)
                f.close()

            xml_assinado = self.enviaComando_acbr(
                'NFE.AssinarNFe({0})'.format(arquivo_xml_local))

            return xml_assinado
        finally:
            self.conexao.close()

    def nfe_envia(self, arquivoxml):
        try:

            arquivo_xml_local = os.getcwd() + '\\envia-nfe.xml'
            # arquivo_xml_local = '''C:\Users\Rodrigo\projetos\pyutil\earquivo_xml.xml'''

            with open(arquivo_xml_local, 'w') as f:
                f.write(arquivoxml)
                f.close()

            assina = '1'  # 1 para assinar, 0 para nao assinar
            nLote = 3500
            retorno = self.enviaComando_acbr(
                'NFE.EnviarNFe({arqXml},{nLote},{assina},{imprimeDanfe}, ,{sincrono})'.format(arqXml=arquivo_xml_local,
                                                                                            nLote=nLote,
                                                                                            assina=assina,
                                                                                            imprimeDanfe='0',
                                                                                            sincrono='1'))
            if 'OK: Lote recebido com sucesso' in retorno:
                return retorno
            raise Exception(retorno)
        finally:
            self.conexao.close()

    def nfe_imprime_danfe(self, dados):
        try:
            arquivo_xml_local = os.getcwd() + '\\nfe.xml'
            with open(arquivo_xml_local, 'w') as f:
                f.write(dados.get('xml'))
                f.close()

            retorno = self.enviaComando_acbr('NFE.ImprimirDanfe("{0}")'.format(arquivo_xml_local))
            if dados.get('dados', {}).get('comprovante_cartao'):
                self.nfe_imprime_relatorio(dados.get('dados', {}).get('comprovante_cartao'))

            return 'Danfe Impresso com sucesso' in retorno
        finally:
            self.conexao.close()

    def nfe_imprime_relatorio(self, texto):
        try:
            retorno = self.enviaComando_acbr('NFE.ImprimirRelatorio("{0}")'.format(texto))
            return 'OK:' in retorno
        finally:
            self.conexao.close()

    def gaveta_abrir(self):
        try:
            self.enviaComando_acbr('GAV.AbreGaveta')
        finally:
            self.conexao.close()

    def tef_crt(self, dados):
        tef = Tef()
        tef.inicializa(tef.PAYGO)

        # Dados da automacao comercial
        tef.nomeEmpresaAutomacaoComercial = dados.get('automacao_nome_empresa')
        tef.nomeAutomacaoComercial = dados.get('automacao_nome_software')
        tef.versaoAutomacaoComercial = dados.get('automacao_versao_software')
        tef.registroCertificacao = dados.get('automacao_registro_certificacao')

        transacaoVenda = TransacaoVenda()

        transacaoVenda.id = dados.get('transacao_id') or 12151
        transacaoVenda.dataHoraCupomfiscal = datetime.now()
        transacaoVenda.doc = dados.get('numero_documento')
        transacaoVenda.valorTransacao = dados.get('valor')
        retorno = tef.crt(transacaoVenda)
        return retorno

    def tef_adm(self, dados):
        tef = Tef()
        tef.inicializa(tef.PAYGO)

        # Dados da automacao comercial
        tef.nomeEmpresaAutomacaoComercial = dados.get('automacao_nome_empresa')
        tef.nomeAutomacaoComercial = dados.get('automacao_nome_software')
        tef.versaoAutomacaoComercial = dados.get('automacao_versao_software')
        tef.registroCertificacao = dados.get('automacao_registro_certificacao')

        numDocFiscal = dados.get('numero_documento')
        idTransacao = dados.get('transacao_id') or 12151
        retorno = tef.adm(idTransacao, numDocFiscal)
        return retorno

    def tef_ncn(self, dados):
        tef = Tef()
        tef.inicializa(tef.PAYGO)

        # Dados da automacao comercial
        tef.nomeEmpresaAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_empresa')
        tef.nomeAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_software')
        tef.versaoAutomacaoComercial = dados.get('dados_automacao').get('automacao_versao_software')
        tef.registroCertificacao = dados.get('dados_automacao').get('automacao_registro_certificacao')

        retorno = tef.ncn(transacoes=dados.get('transacoes'))
        return retorno

    def tef_cnf(self, dados):
        """Confirmacao nao fical"""
        tef = Tef()
        tef.inicializa(tef.PAYGO)

        # Dados da automacao comercial
        tef.nomeEmpresaAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_empresa')
        tef.nomeAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_software')
        tef.versaoAutomacaoComercial = dados.get('dados_automacao').get('automacao_versao_software')
        tef.registroCertificacao = dados.get('dados_automacao').get('automacao_registro_certificacao')

        retorno = tef.cnf(transacoes=dados.get('transacoes'))
        return retorno
