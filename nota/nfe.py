import os
import socket
from time import sleep

__author__ = 'Rodrigo'


def formataValor(valor):
    subtotal = valor.replace(',', '.').strip()
    subtotal = float(subtotal)
    return subtotal


class Socket:
    def __init__(self):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host, port):
        self.mySocket.connect((host, port))
        # self.mySocket.listen(1)

    def send(self, command):
        self.mySocket.sendall(command.encode())
        self.mySocket.settimeout(3)
        data = ''
        while True:
            try:
                # this is the problem here
                data += self.mySocket.recv(1024).decode()
                if 'OK:' in data:
                    return data

            except Exception as e:
                return str(e)

    def close(self):
        self.mySocket.close()


class Nfe:
    def enviaComando_acbr(self, comando, conexao):
        retorno = conexao.send(str(comando + '\r\n.\r\n'))

        #
        # if isinstance(retorno, list):
        #     for s in retorno:
        #         retornoNovo = ''
        #         if ord(s) >= 32:
        #             retornoNovo += s
        #
        #         retorno = retornoNovo

        return retorno

    def assina_xml(self, arquivoxml):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)
        try:

            arquivo_xml_local = os.getcwd() + '\\teste-nfe.xml'
            # arquivo_xml_local = '''C:\Users\Rodrigo\projetos\pyutil\earquivo_xml.xml'''

            with open(arquivo_xml_local, 'w') as f:
                f.write(arquivoxml)
                f.close()

            xml_assinado = self.enviaComando_acbr(
                'NFE.AssinarNFe({0})'.format(arquivo_xml_local), conexao)

            return xml_assinado
        finally:
            conexao.close()

    def imprime_danfe(self, arquivoxml):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)
        try:
            arquivo_xml_local = os.getcwd() + '\\teste-nfe.xml'
            # arquivo_xml_local = '''C:\Users\Rodrigo\projetos\pyutil\earquivo_xml.xml'''

            with open(arquivo_xml_local, 'w') as f:
                f.write(arquivoxml)
                f.close()

            retorno = self.enviaComando_acbr('NFE.ImprimirDanfe("{0}")'.format(arquivo_xml_local), conexao)

            return 'Danfe Impresso com sucesso' in retorno.decode()
        finally:
            conexao.close()

    def imprime_relatorio(self, texto):
        conexao = Socket()
        conexao.connect('127.0.0.1', 3434)
        try:
            retorno = self.enviaComando_acbr('NFE.ImprimirRelatorio("{0}")'.format(texto), conexao)
            return 'OK:' in retorno
        finally:
            conexao.close()

