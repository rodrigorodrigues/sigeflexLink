import ntpath
import os
import py_compile
from shutil import copyfile

files = ['manage.py', 'geral/sigelink.py',
         'pytef/tef.py',
         'pytef/tef_paygo.py',
         'pytef/__init__.py',
         'sigelink/__init__.py',
         'sigelink/urls.py',
         'sigelink/wsgi.py',
         'sigelink/settings.py',
         'geral/__init__.py',
         'geral/sigelink.py',
         'geral/views.py',
         ]

path = os.getcwd() + '\\dist_django\\'
for file in files:
    filename = ntpath.basename(file)
    path2 = path + os.path.dirname(file)
    if not os.path.exists(path2):
        os.makedirs(path2)

    py_compile.compile(file, cfile=path2 + '\\' + filename + 'c')

copyfile('run.bat', 'dist_django/run.bat')
