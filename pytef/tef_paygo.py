# -*- coding: utf-8 -*-
import datetime
import os
from time import sleep

__author__ = 'Rodrigo Rodrigues'

ARQUIVO_TEMP = 'C:\TEF_Dial\\req\intpos.tmp'
ARQUIVO_REQ = 'C:\TEF_Dial\\req\intpos.001'
ARQUIVO_RESP = 'C:\TEF_Dial\\resp\intpos.001'
ARQUIVO_STATUS = 'C:\TEF_Dial\\resp\intpos.sts'

ERRO_MENSAGEM_TEF_NAO_RESPODE = 'TEF não responde'
ERRO_MENSAGEM_INCONSISTENCIA = 'Inconsistência no campo NUMEROCAMPO do arquivo NOMEARQUIVO gerado pelo TEF'
ERRO_MENSAGEM_TRANSACAO_CANCELADA = '''
                                    Transação TEF cancelada:
                                    Rede: <REDE>
                                    NSU: <NSU>
                                    Valor: <VALOR>
                                    '''


def grava_log(acao, mensagem):
    with open('tef.log', 'a+') as f:
        linha = '{datahora} - {acao} - {mensagem}'.format(
            datahora=datetime.datetime.now().strftime('%d/%m/%Y %H:%m:%S'),
            acao=acao,
            mensagem=mensagem)
        linha = f.read() + '\n' + linha
        f.write(linha)
        f.close()


class Requisicao():
    def __init__(self, tef):
        self.tef = tef
        self.requisicao = []

    def processa_resposta(self, mapeamentoCampos, resposta):
        mapeamento = {}
        try:
            for linha in resposta:
                campo = linha[:7]
                for key in mapeamentoCampos:
                    if mapeamentoCampos[key] == campo:
                        mapeamento[key] = linha[10:]

                        # Preenche campos XXX que podem variar de tamanho, por exeplo, a via do cliente, estabelecimento etc
                        if key == 'VIA_CLIENTE' or \
                                key == 'VIA_ESTABELECIMENTO' or \
                                key == 'VIA_UNICA' or \
                                key == 'VIA_CUPOM_REDUZIDO' or \
                                key == 'VIA_UNICA_COMPROVANTE' or \
                                key == 'VIA_ESTEBALECIMENTO_COMPROVANTE':
                            mapeamento[key] = int(mapeamento[key])
                            camposAFiltrar = int(campo[:3]) + 1
                            camposAFiltrar = str(camposAFiltrar).zfill(3)
                            novalista = list(filter(lambda x: x[:3] == camposAFiltrar, resposta))
                            mapeamento[key] = []
                            for n in novalista:
                                mapeamento[key].append(n[10:])

            return mapeamento
        except Exception as e:
            grava_log('processa_resposta',
                      'TEF Retornou uma resposta inesperada : Resposta : ' + str(resposta) + ' Erro : ' + str(e))

    def add_campo(self, campo, valor):
        """
        :param campo: O campo a ser adcionado a requisicao
        :param valor: O valor que será adcionado ao campo
        :return: Sem retonro
        """
        self.requisicao.append(campo + ' = ' + str(valor))

    def exclui_arquivo(self, filename):
        try:
            os.remove(filename)
        except OSError:
            pass

    def remove_pasta_resp(self):
        """  Essa funçao remove os arquivos contidos na pasta resp, necessario para realizar uma nova transacao  """

        self.exclui_arquivo(ARQUIVO_TEMP)
        self.exclui_arquivo(ARQUIVO_RESP)
        self.exclui_arquivo(ARQUIVO_STATUS)

    def valida_arquivo_resposta(self, resposta):
        """ Confere se o arquivo de resposta tem o mesmo conteudo do aruqivo de solicitacao, caso contrario, acuse o erro   """

        respostaSts = self.processa_resposta(CAMPOS, resposta)
        arquivoIntpos = self.processa_resposta(CAMPOS, self.requisicao)

        mensagemOperadorOK = ''
        verificarCampos = ['COMANDO', 'ID_TRANSACAO']
        for campo in verificarCampos:
            try:
                if respostaSts[campo] != arquivoIntpos[campo]:
                    raise ValueError

            except ValueError:
                mensagemOperadorOK += ERRO_MENSAGEM_INCONSISTENCIA.format(NUMEROCAMPO=campo,
                                                                          NOMEARQUIVO='intpos.sts') + '\n'

        if mensagemOperadorOK:
            if hasattr(self.tef.tef, 'onMensagemOK'):
                self.tef.tef.onMensagemOK(ERRO_MENSAGEM_INCONSISTENCIA)

            return False

        return True

    def aguarda_resposta_sts(self):
        """
        Essa funcao aguarda o retorno do arquivo intpos.sts que é o arquivo de confirmacao do TEF, quando envia um arquivo intpos.001 o tef responde com intpos.sts com o mesmo conteúdo do arquivo de
        solicitacao(intpos.001)
        - Caso o tef não retorne em até 7 segundos é preciso gerar um erro e a automacao comercial mostrará esse erro
        - Caso o arquivo intpos.sts não seja igual ao arquivo intpos.001, então a automação comercial deve informar o erro mostrando os campos que estão inconsistente bem como o nome do arquivo
        """
        timeout = 1
        while timeout <= 7:
            try:
                file = open(ARQUIVO_STATUS)
                resposta = [line.rstrip('\n') for line in file]
                file.close()
                if self.valida_arquivo_resposta(resposta):
                    return resposta

            except IOError as e:
                timeout += 1

            sleep(1)

        # Caso o tef nao tenha respondido a requisicao em 7 segundos
        if timeout > 7:
            grava_log('aguarda_resposta_sts',
                      'TEF nao retornou arquivo intpos.sts que é o arquivo de confirmacao do TEF ')

            return False

    def aguarda_resposta_intpos001(self):
        """ ler o arquivo de resposta, só saia do laco quando tiver a resposta ou der timeout """
        resposta = None
        timeout = 1
        while timeout:
            try:
                file = open(ARQUIVO_RESP)
                resposta = [line.replace('"', '').rstrip('\n') for line in file]
                file.close()
                timeout = 0
            except IOError as e:
                timeout += 1
                if timeout == 180:
                    grava_log('aguarda_resposta_intpos001',
                              'O TEF nao respondeu com o arquivo intpos001 em um periodo de 180 segundos')
                    break
            sleep(1)

        if resposta:
            resposta = self.processa_resposta(CAMPOS, resposta)
            return resposta
        else:
            return "O TEF nao respondeu"

    def reqIntpos001(self):
        try:
            # apaga conteúdo da pasta resp
            self.remove_pasta_resp()

            # Adiciona o registro finalizador
            self.add_campo(CAMPOS['FINAL'], '0')

            # salva o arquivo de requisicao
            text_file = open(ARQUIVO_TEMP, "w")
            for campo in self.requisicao:
                text_file.write(campo + '\n')
            text_file.close()

            if os.path.exists(ARQUIVO_REQ):
                os.remove(ARQUIVO_REQ)
            # Nesse momento, o TEF é para ler o arqivo e o apagar
            os.rename(ARQUIVO_TEMP, ARQUIVO_REQ)

            # Verifica se o TEF respondeu a requisicao do req, ou seja, verifica se existe o arquivo intpos.sts
            return self.aguarda_resposta_sts()
        except Exception as e:
            grava_log('reqIntpos001',
                      'Erro ao realizar requisicao Erro : ' + str(e))

    def confirma_transacao(self, resposta):
        if resposta['STATUS_CONFIRMACAO'] != '1':  # Se a transacao foi confirmada
            self.remove_pasta_resp()
            self.tef.cnf(resposta['REDE_ADQUIRENTE'], resposta['CODIGO_CONTROLE'], resposta['DOCUMENTO_FISCAL'],
                         resposta['ID_TRANSACAO'])

    def cancela_transacao(self, dados):
        """
            Se a transacao NAO FOI CONFIRMADA, execute a funcao da AC para mostrar ao usuario a mensagem
            caso contrário, TRANSACAO CONFIRMADA, envie um comando de ncn (CONFIRMACAO)
        """
        if dados['STATUS_CONFIRMACAO'] == '1':
            mensagemOperadorOK = ERRO_MENSAGEM_TRANSACAO_CANCELADA.format(REDE=dados['REDE_ADQUIRENTE'],
                                                                          NSU=dados['NSU'],
                                                                          VALOR=dados['VALOR_TRANSACAO'])
            if hasattr(self.tef.tef, 'onMensagemOK'):
                self.tef.tef.onMensagemOK()

            return mensagemOperadorOK
        else:
            self.remove_pasta_resp()
            self.tef.ncn(dados['REDE_ADQUIRENTE'], dados['CODIGO_CONTROLE'], dados['DOCUMENTO_FISCAL'],
                         dados['ID_TRANSACAO'])


class TefPaygo():
    def __init__(self, tef):
        self.tef = tef

    # Comando de ativacao TEF
    def atv(self):
        req = Requisicao(self)
        req.add_campo(CAMPOS['COMANDO'], 'ATV')
        req.add_campo(CAMPOS['ID_TRANSACAO'], self.tef.onGetIdTransacao())
        req.add_campo(CAMPOS['VERSAO_INTERFACE'], '210')
        req.add_campo(CAMPOS['REGISTRO_CERTIFICACAO'], self.tef.registroCertificacao)
        req.reqIntpos001()

    # Transacao adaministrativa
    def adm(self, idTransacao, numDocFiscal):

        req = Requisicao(self)
        req.add_campo(CAMPOS['COMANDO'], 'ADM')
        req.add_campo(CAMPOS['ID_TRANSACAO'], idTransacao)
        req.add_campo(CAMPOS['DOCUMENTO_FISCAL'], numDocFiscal)
        resposta = req.reqIntpos001()
        if not resposta:
            return 'O TEF não respondeu'

        resposta = req.aguarda_resposta_intpos001()

        # No caso do TEF nao ter respondido
        if isinstance(resposta, str):
            return resposta

        if resposta['STATUS'] != '0':
            return resposta['MENSAGEM_OPERADOR']

        if hasattr(self.tef, 'onMensagemOperador'):
            self.tef.onMensagemOperador(resposta['MENSAGEM_OPERADOR'])
        try:
            if resposta['VIAS_COMPROVANTE']:  # Se em algo para imprimir
                if hasattr(self, 'onResposta'):
                    if self.onResposta(resposta):  # Se imprimiu o comprovante
                        self.tef.confirma_transacao(resposta)
                else:
                    self.tef.confirma_transacao(resposta)
        except:
            pass
        return resposta

    # Comando de venda, realizar uma transacao
    def crt(self, transacaoVenda):

        req = Requisicao(self)
        req.add_campo(CAMPOS['COMANDO'], 'CRT')
        req.add_campo(CAMPOS['ID_TRANSACAO'], transacaoVenda.id)
        req.add_campo(CAMPOS['DOCUMENTO_FISCAL'], transacaoVenda.doc)
        req.add_campo(CAMPOS['VALOR_TRANSACAO'], format(transacaoVenda.valorTransacao, '.2f').replace('.', ''))
        req.add_campo(CAMPOS['MOEDA'], transacaoVenda.moeda)  # 0 real 1 dolar americano

        # Se indentifica o cliente
        if transacaoVenda.cpf:
            req.add_campo(CAMPOS['ENTIDADE_CLIENTE'], '0')
            req.add_campo(CAMPOS['CPF_CNPJ_CLIENTE'], transacaoVenda.cpf)
        elif transacaoVenda.cnpj:
            req.add_campo(CAMPOS['ENTIDADE_CLIENTE'], '1')
            req.add_campo(CAMPOS['CPF_CNPJ_CLIENTE'], transacaoVenda.cnpj)

        if transacaoVenda.rede:
            req.add_campo(CAMPOS['REDE_ADQUIRENTE'], transacaoVenda.rede)

        # Se foi especificado o tipo de parcelamento pela automacao comercial
        if transacaoVenda.tipoParcelamento and transacaoVenda.parcelas > 0:
            if transacaoVenda.tipoParcelamento == 'avista':
                req.add_campo(CAMPOS['TIPO_PARCELAMENTO'], '1')
            elif transacaoVenda.tipoParcelamento == 'parceladoEmissor':
                req.add_campo(CAMPOS['TIPO_PARCELAMENTO'], '2')
            elif transacaoVenda.tipoParcelamento == 'parceladoEstabelecimento':
                req.add_campo(CAMPOS['TIPO_PARCELAMENTO'], '3')
            elif transacaoVenda.tipoParcelamento == 'preDatado':
                req.add_campo(CAMPOS['TIPO_PARCELAMENTO'], '4')
            elif transacaoVenda.tipoParcelamento == 'preDatadoForcado':
                req.add_campo(CAMPOS['TIPO_PARCELAMENTO'], '5')

            req.add_campo(CAMPOS['QTDE_PARCELAS'], str(transacaoVenda.parcelas))

            # Se a transacao foi programada para uma determinada data
            if transacaoVenda.dataPredatada:
                req.add_campo(CAMPOS['DATA_PRE_DATADO'], transacaoVenda.dataPredatada.strftime('%d%m%Y'))

        # 2:funcionalidade de desconto (ver campo 709-000) +
        # 4: valor fixo, sempre incluir +
        # 8 vias diferenciadas do comprovante para
        # Cliente/Estabelecimento (campos 712-000 a 715-000) +
        # 16: cupom reduzido (campos 710-000 e 711-000)
        # a soma dessas funcionalidades é igual a 30
        req.add_campo(CAMPOS['CAPACIDADE_AUTOMACAO'], 3)

        req.add_campo(CAMPOS['EMPRESA_AUTOMACAO'], self.tef.nomeEmpresaAutomacaoComercial)

        req.add_campo(CAMPOS['DATA_HORA_CUPOM_FISCAL'], transacaoVenda.dataHoraCupomfiscal.strftime('%y%m%d%H%M%S'))

        # Se foi definido o tipo de cartao
        if transacaoVenda.tipoCartao:
            if transacaoVenda.tipoCartao == 'outro':
                req.add_campo(CAMPOS['TIPO_CARTAO'], '0')
            elif transacaoVenda.tipoCartao == 'credito':
                req.add_campo(CAMPOS['TIPO_CARTAO'], '1')
            elif transacaoVenda.tipoCartao == 'debito':
                req.add_campo(CAMPOS['TIPO_CARTAO'], '2')
            elif transacaoVenda.tipoCartao == 'voucher':
                req.add_campo(CAMPOS['TIPO_CARTAO'], '3')

        req.add_campo(CAMPOS['VERSAO_INTERFACE'], '210')
        req.add_campo(CAMPOS['NOME_AUTOMACAO_COMERCIAL'], self.tef.nomeAutomacaoComercial)
        req.add_campo(CAMPOS['VERSAO_AUTOMACAO'], self.tef.versaoAutomacaoComercial)
        req.add_campo(CAMPOS['REGISTRO_CERTIFICACAO'], self.tef.registroCertificacao)
        resposta = req.reqIntpos001()
        if not resposta:
            raise Exception("O TEF não respondeu a requisicao!")

        resposta = req.aguarda_resposta_intpos001()

        return resposta

    def cnf(self, transacoes):
        """
        Confirmacao de transacao
        :param rede:
        :param codigoControle:
        :param doc:
        :param idTransacao:
        :return:
        """
        retorno_transacoes = []
        for transacao in transacoes:
            req = Requisicao(self)
            req.add_campo(CAMPOS['COMANDO'], 'CNF')
            req.add_campo(CAMPOS['ID_TRANSACAO'], transacao.get('transacao_id'))
            if transacao.get('doc'):
                req.add_campo(CAMPOS['DOCUMENTO_FISCAL'], transacao.get('doc'))
            req.add_campo(CAMPOS['REDE_ADQUIRENTE'], transacao.get('rede'))
            req.add_campo(CAMPOS['CODIGO_CONTROLE'], transacao.get('codigo_controle'))
            req.add_campo(CAMPOS['VERSAO_INTERFACE'], '210')
            req.add_campo(CAMPOS['NOME_AUTOMACAO_COMERCIAL'], self.tef.nomeAutomacaoComercial)
            req.add_campo(CAMPOS['VERSAO_AUTOMACAO'], self.tef.versaoAutomacaoComercial)
            req.add_campo(CAMPOS['REGISTRO_CERTIFICACAO'], self.tef.registroCertificacao)
            req.reqIntpos001()
        return retorno_transacoes

    def ncn(self, transacoes):
        """
        Realiza um cancelamento de transacao
        :param id_transacao:
        :param num_doc:
        :param rede:
        :param codigo_controle:
        :param indiceEstabelecimento:
        :param dataHoraCupomfiscal:
        :return:
        """
        retorno_transacoes = []
        for transacao in transacoes:
            req = Requisicao(self)
            req.add_campo(CAMPOS['COMANDO'], 'NCN')
            req.add_campo(CAMPOS['ID_TRANSACAO'], transacao.get('transacao_id'))
            # req.add_campo(CAMPOS['DOCUMENTO_FISCAL'], num_doc)
            req.add_campo(CAMPOS['REDE_ADQUIRENTE'], transacao.get('rede'))
            req.add_campo(CAMPOS['CODIGO_CONTROLE'], transacao.get('codigo_controle'))
            # req.add_campo(CAMPOS['INDICE_ESTABELECIMENTO'], indiceEstabelecimento)
            # req.add_campo(CAMPOS['DATA_HORA_CUPOM_FISCAL'], dataHoraCupomfiscal.strftime('%Y%m%d%H%M%S'))
            req.add_campo(CAMPOS['VERSAO_INTERFACE'], '210')
            req.add_campo(CAMPOS['REGISTRO_CERTIFICACAO'], self.tef.registroCertificacao)
            req.add_campo(CAMPOS['INDICE_REDE_ADQUIRENTE'], transacao.get('indice_rede_adquirente'))
            retorno = req.reqIntpos001()
            # resposta = req.aguarda_resposta_intpos001()
            retorno_transacoes.append(retorno)

        return retorno_transacoes


CAMPOS = {'COMANDO': '000-000',
          'ID_TRANSACAO': '001-000',
          'DOCUMENTO_FISCAL': '002-000',
          'VALOR_TRANSACAO': '003-000',
          'MOEDA': '004-000',
          'ENTIDADE_CLIENTE': '006-000',
          'CPF_CNPJ_CLIENTE': '007-000',
          'STATUS': '009-000',
          'REDE_ADQUIRENTE': '010-000',
          'INDICE_REDE_ADQUIRENTE': '739-000',
          'NSU': '012-000',
          'CODIGO_AUTORIZACAO': '013-000',
          'QTDE_PARCELAS': '018-000',
          'DATA_COMPROVANTE': '022-000',
          'HORA_COMPROVANTE': '023-000',
          'DATA_PRE_DATADO': '024-000',
          'NSU_ORIGINAL': '025-000',
          'DATA_HORA_REDE_ORIGINAL': '026-000',
          'CODIGO_CONTROLE': '027-000',
          'VIA_UNICA': '028-000',
          'VIA_UNICA_COMPROVANTE': '029-xxx',
          'MENSAGEM_OPERADOR': '030-000',
          'NOME_CARTAO_OU_ADMINISTRADORA': '040-000',
          'INDICE_ESTABELECIMENTO': '702-000',
          'CAPACIDADE_AUTOMACAO': '706-000',
          'VALOR_ORIGINAL': '707-000',
          'VALOR_TROCO': '708-000',
          'VALOR_DESCONTO': '709-000',
          'VIA_CUPOM_REDUZIDO': '710-000',
          'CUPOM_REDUZIDO': '711-xxx',
          'VIA_CLIENTE': '712-000',
          'VIA_CLIENTE_COMPROVANTE': '713-000',
          'VIA_ESTABELECIMENTO': '714-000',
          'VIA_ESTEBALECIMENTO_COMPROVANTE': '715-xxx',
          'EMPRESA_AUTOMACAO': '716-000',
          'DATA_HORA_CUPOM_FISCAL': '717-000',
          'NUMERO_LOGICO_TERMINAL': '718-000',
          'CODIGO_ESTABALECIMENTO': '719-000',
          'DADOS_ADICIONAIS1': '722-000',
          'DADOS_ADICIONAIS2': '723-000',
          'DADOS_ADICIONAIS3': '724-000',
          'DADOS_ADICIONAIS4': '725-000',
          'TAXA_SERVICO': '727-000',
          'TAXA_EMBARQUE': '728-000',
          'STATUS_CONFIRMACAO': '729-000',
          'OPERACAO': '730-000',  # VER TABELA ACIMA
          'TIPO_CARTAO': '731-000',
          'TIPO_PARCELAMENTO': '732-000',
          'VERSAO_INTERFACE': '733-000',  # VERSAO DO DOCUMENTO DE INTEGRACAO SEGUIDO PARA FAZER A HOMOLOGACAO
          'GRUPO_TRANSACAO': '734-000',
          'NOME_AUTOMACAO_COMERCIAL': '735-000',
          'VERSAO_AUTOMACAO': '736-000',
          'VIAS_COMPROVANTE': '737-000',
          'REGISTRO_CERTIFICACAO': '738-000',
          'FINAL': '999-999'
          }
