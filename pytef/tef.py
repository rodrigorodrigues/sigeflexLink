from .tef_paygo import TefPaygo

__author__ = 'Rodrigo'

class Tef(object):
    tef = None
    PAYGO = 'PAYGO'

    def __init__(self):
        self.nomeAutomacaoComercial = ''
        self.versaoAutomacaoComercial = ''
        self.registroCertificacao = ''

    def inicializa(self, tipoTef):
        if tipoTef == self.PAYGO:
            self.tef = TefPaygo(self)

    def atv(self):
        return self.tef.atv()

    def adm(self, idTransacao, numDocFiscal):
        retorno = self.tef.adm(idTransacao, numDocFiscal)
        return retorno

    def crt(self, transacaoVenda):
        return self.tef.crt(transacaoVenda)

    def ncn(self, transacoes):
        return self.tef.ncn(transacoes)

    def cnf(self, transacoes):
        return self.tef.cnf(transacoes)


class TransacaoVenda:
    def __init__(self):
        self.id = ''
        self.doc = ''
        self.valorTransacao = 0
        self.moeda = '0'
        self.cpf = ''
        self.cnpj = ''
        self.rede = ''
        self.tipoParcelamento = ''
        self.parcelas = 0
        self.dataPredatada = None
        self.dataHoraCupomfiscal = None
        self.tipoCartao = ''
