from datetime import datetime
from random import randint

from ..tef import Tef, TransacaoVenda

__author__ = 'Rodrigo'



#Aqui a aplicacao comercial deve fazer todo o tratamento de queda de energia
def imprimeComprovante(dados):
    retorno = dados

    if 'VIA_CLIENTE' in dados:
       pass

    if 'VIA_CUPOM_REDUZIDO' in dados:
        pass

    if 'VIA_ESTABELECIMENTO' in dados:
        pass

    return True


def getIdTransacao():
    return randint(1, 999999)


def mensagemOperador(mensagem):
    print(mensagem)


def mensagemOK(mensagem):
    print(mensagem)

tef = Tef()
tef.inicializa(tef.PAYGO)

# funcao acioanada pelo tef para imprimir os comprovanes
tef.onResposta = imprimeComprovante

# funcao acionada pelo tef para a automacao comercial gerar um ID de identificacao
tef.onGetIdTransacao = getIdTransacao

# funcao acionada pelo tef para mostrar mensagens ao operador
tef.onMensagemOperador = mensagemOperador

#funcao acionada pelo tef para mostrar a mensagem ao operador que necessite do OK do operador, o processo TEF fica parado até que o operador clique em OK
tef.onMensagemOK = mensagemOK


# Dados da automacao comercial
tef.nomeEmpresaAutomacaoComercial = 'BIT AUTOMACAO LTDA - ME'
tef.nomeAutomacaoComercial = 'PDVFlex'
tef.versaoAutomacaoComercial = 'Versao 2015'
tef.registroCertificacao = 'ABC21152266411'


# #Testa uma venda no cartao
# def test_crt_venda_999999_99():
#
#
#     transacaoVenda = TransacaoVenda()
#     transacaoVenda.id = '52363'
#     transacaoVenda.dataHoraCupomfiscal = datetime.now()
#     transacaoVenda.doc = '541236'
#     transacaoVenda.valorTransacao = 150.85
#
#     retorno = tef.crt(transacaoVenda)
#
#
#     assert retorno.get('DOCUMENTO_FISCAL') == '541236'
#     assert retorno.get('VALOR_TRANSACAO') == '15085'
#     assert 'AUTORIZADA' in retorno.get('MENSAGEM_OPERADOR')


#Testa uma venda no cartao
def test_crt_venda_999999_99():


    transacaoVenda = TransacaoVenda()
    transacaoVenda.id = '52363'
    transacaoVenda.dataHoraCupomfiscal = datetime.now()
    transacaoVenda.doc = '541236'
    transacaoVenda.valorTransacao = 999999.99

    retorno = tef.crt(transacaoVenda)


    assert retorno.get('DOCUMENTO_FISCAL') == '541236'
    assert retorno.get('VALOR_TRANSACAO') == '99999999'
    assert 'AUTORIZADA' in retorno.get('MENSAGEM_OPERADOR')

def test_crt_operacao_cancelada():

    transacaoVenda = TransacaoVenda()
    transacaoVenda.id = '54541'
    transacaoVenda.dataHoraCupomfiscal = datetime.now()
    transacaoVenda.doc = '9985'
    transacaoVenda.valorTransacao = 150

    retorno = tef.crt(transacaoVenda)
    assert retorno.get('MENSAGEM_OPERADOR') == 'OPERACAO CANCELADA'

# Faz um teste de uma transacao administrativa
def test_adm():
    numDocFiscal = '1234'
    idTransacao = '6854'
    retorno = tef.adm(idTransacao, numDocFiscal)

    assert retorno.get('DOCUMENTO_FISCAL') == '1234'
    assert retorno.get('ID_TRANSACAO') == '6854'


