from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^tef/crt$', views.tef_crt, name='crt'),
    url(r'^tef/ncn$', views.tef_ncn, name='ncn'),
    url(r'^tef/cnf', views.tef_cnf, name='cnf'),
    url(r'^tef/adm$', views.tef_adm, name='adm'),
    # url(r'^', include(router.urls)),
]
