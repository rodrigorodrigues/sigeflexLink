from datetime import datetime
import json

from django.shortcuts import render

# Create your views here.


#
from pytef.tef import Tef, TransacaoVenda

def tef_teste(request):
    return "Ola"

def tef_crt(request):
    dados = request.get_json(silent=True)

    tef = Tef()
    tef.inicializa(tef.PAYGO)

    # Dados da automacao comercial
    tef.nomeEmpresaAutomacaoComercial = dados.get('automacao_nome_empresa')
    tef.nomeAutomacaoComercial = dados.get('automacao_nome_software')
    tef.versaoAutomacaoComercial = dados.get('automacao_versao_software')
    tef.registroCertificacao = dados.get('automacao_registro_certificacao')

    transacaoVenda = TransacaoVenda()

    transacaoVenda.id = dados.get('transacao_id') or 12151
    transacaoVenda.dataHoraCupomfiscal = datetime.now()
    transacaoVenda.doc = dados.get('numero_documento')
    transacaoVenda.valorTransacao = dados.get('valor')
    retorno = tef.crt(transacaoVenda)

    return json.dumps(retorno)



def tef_adm(request):
    dados = request.get_json(silent=True)

    tef = Tef()
    tef.inicializa(tef.PAYGO)

    # Dados da automacao comercial
    tef.nomeEmpresaAutomacaoComercial = dados.get('automacao_nome_empresa')
    tef.nomeAutomacaoComercial = dados.get('automacao_nome_software')
    tef.versaoAutomacaoComercial = dados.get('automacao_versao_software')
    tef.registroCertificacao = dados.get('automacao_registro_certificacao')

    numDocFiscal = dados.get('numero_documento')
    idTransacao = dados.get('transacao_id') or 12151
    retorno = tef.adm(idTransacao, numDocFiscal)


    return json.dumps(retorno)



def tef_ncn(request):
    dados = request.get_json(silent=True)

    tef = Tef()
    tef.inicializa(tef.PAYGO)

    # Dados da automacao comercial
    tef.nomeEmpresaAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_empresa')
    tef.nomeAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_software')
    tef.versaoAutomacaoComercial = dados.get('dados_automacao').get('automacao_versao_software')
    tef.registroCertificacao = dados.get('dados_automacao').get('automacao_registro_certificacao')

    retorno = tef.ncn(transacoes=dados.get('transacoes'))

    return json.dumps(retorno)



def tef_cnf(request):
    dados = request.get_json(silent=True)

    tef = Tef()
    tef.inicializa(tef.PAYGO)

    # Dados da automacao comercial
    tef.nomeEmpresaAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_empresa')
    tef.nomeAutomacaoComercial = dados.get('dados_automacao').get('automacao_nome_software')
    tef.versaoAutomacaoComercial = dados.get('dados_automacao').get('automacao_versao_software')
    tef.registroCertificacao = dados.get('dados_automacao').get('automacao_registro_certificacao')

    retorno = tef.cnf(transacoes=dados.get('transacoes'))

    return json.dumps(retorno)